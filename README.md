# arachnope overview
An Arachni loot parser to automate No/My/SQLi pen-testing.

# install
``` sh
git clone https://gitlab.com/aceldama/arachnope.git
cd arachnope
chmod +x arachnope.py
```

# optional (to use arachnope in any directory)
``` sh
sudo ln -s "$(pwd)/arachnope.py" /usr/bin
```

# sanity checks
## example
``` sh
./arachnope --help
```

## output
```
                       *
              *              )
   (                    ) ( /(       *
 * )\   (      )     ( /( )\())            (
((((_)( )(  ( /(  (  )\()|(_)\  (  `  )   ))\
 )\ _ )(()\ )(_)) )\((_)\ _((_) )\ /(/(  /((_)
 (_)_\(_|(_|(_)_ ((_) |(_) \| |((_|(_)_\(_))
  / _ \| '_/ _` / _|| ' \| .` / _ \ '_ \) -_)
 /_/ \_\_| \__,_\__||_||_|_|\_\___/ .__/\___|
                                  |_|
    v0.1.0 (05/05/2020)
                               -- by AceldamA;



    USAGE:
        ./arachnope.py [flags] inputfile

    FLAGS:
        --help              : Prints this help page.
        --user-agent <str>  : Sets the User-Agent header
        --parse-only        : Prints the NoSQL entries without testing them.
        --sample <int>      : Specifies how many sample pages to request for blind injections. (Default: 3)
        --sqlmap            : [TO DO] Performs sqlmap automatically with the given command.

    EXAMPLE:
        ./arachnope.py --print-only "/tmp/arachni.txt"
        ./arachnope.py "/tmp/arachni.txt"
```
