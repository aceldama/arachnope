#!/usr/bin/env python3
import os
import sys


#---------------------------------------------------------------------------------------------------------[ Globals ]--
RELEASE_AUTHOR = "AceldamA"
RELEASE_VERSION = "0.1.0"
RELEASE_DATE = "05/05/2020"

FLAG_DEFAULTS = {
    "--sample"      : 3,
    "--user-agent"  : "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0",
    "--parse-only"  : True
}
FLAGS = {
    "--help"        : ( "Prints this help page.", None ),
    "--user-agent"  : ("Sets the User-Agent header", str),
    "--parse-only"  : ( "Prints the NoSQL entries without testing them.", None ),
    "--sample"      : ( "Specifies how many sample pages to request for blind injections. (Default: {})".format(FLAG_DEFAULTS["--sample"]), int ),
    "--sqlmap"      : ( "[TO DO] Performs sqlmap automatically with the given command.", None )
}

SPLASH = """                       *
              *              )                 
   (                    ) ( /(       *         
 * )\   (      )     ( /( )\())            (   
((((_)( )(  ( /(  (  )\()|(_)\  (  `  )   ))\  
 )\ _ )(()\ )(_)) )\((_)\ _((_) )\ /(/(  /((_) 
 (_)_\(_|(_|(_)_ ((_) |(_) \| |((_|(_)_\(_))   
  / _ \| '_/ _` / _|| ' \| .` / _ \ '_ \) -_)  
 /_/ \_\_| \__,_\__||_||_|_|\_\___/ .__/\___|  
                                  |_|          
    v{} ({})  
                               -- by {};

""".format(RELEASE_VERSION, RELEASE_DATE, RELEASE_AUTHOR)


#-------------------------------------------------------------------------------------------------------[ Class(es) ]--
class ArachniSegment():
    """ A class to parse arachni output and make it easy to use in the main arachnope program.
    """
    @property
    def raw(self) -> list:
        """ Raw list of arachni output lines
        """
        return self._raw

    @raw.setter
    def raw(self, value : list):
        #-- Set raw
        self._toStr = None
        self._raw = value

        #-- Parse header line
        self.header = value[0][value[0].index("]") + 1:].strip() 
        self.index = int(value[0][1:value[0].index("]")])
        self.isSQL = " SQL " in value[0]
        self.isNoSQL = " NoSQL " in value[0]
        self.isBlind = " Blind " in value[0]

        #-- Parse properties
        i = 0
        self._http = list()
        self._element = None
        self._injected = None
        self._referer = None
        self._page = None
        self._iTrue = None
        self._iFalse = None
        while i < len(value):
            if value[i] == " [~] HTTP request":
                i += 1
                while (i < len(value)) and (value[i].strip()[0] != "["):
                    self._http.append(value[i])
                    i += 1
            elif value[i].startswith(" [~] Element: "):
                self._element = value[i][value[i].index(":") + 1:].strip()
            elif value[i].startswith(" [~] Injected: "):
                self._injected = value[i][value[i].index(":") + 1:].strip()[1:-1]
            elif value[i].startswith(" [~] Referring page:"):
                self._referer = value[i][value[i].index(":") + 1:].strip()
            elif value[i].startswith(" [~] Affected page:"):
                self._page = value[i][value[i].index(":") + 1:].strip()
            elif " *  True expression: " in value[i]:
                self._iTrue = value[i][value[i].index(":") + 1:].strip()
            elif " *  False expression: " in value[i]:
                self._iFalse = value[i][value[i].index(":") + 1:].strip()
            i += 1

    @property
    def header(self) -> str:
        """ The Arachni Header line without the index
        """
        return self._header

    @header.setter
    def header(self, value : str):
        self._toStr = None
        self._header = value


    @property
    def isSQL(self) -> bool:
        """ True/False SQLi flag
        """
        return self.is_sqli
    @isSQL.setter
    def isSQL(self, value : bool):
        self._toStr = None
        self.is_sqli = value


    @property
    def isNoSQL(self) -> bool:
        """ True/False NoSQLi flag
        """
        return self.is_nosql
    @isNoSQL.setter
    def isNoSQL(self, value : bool):
        self._toStr = None
        self.is_nosql = value


    @property
    def isBlind(self) -> bool:
        """ True/False Blind injection flag
        """
        return self.is_blind
    @isBlind.setter
    def isBlind(self, value):
        self._toStr = None
        self.is_blind = value


    @property
    def index(self) -> int:
        """ The arachni output index
        """
        return self._index
    @index.setter
    def index(self, value : int):
        self._toStr = None
        self._index = value


    def __init__(self, seg_strings: list):
        """ Class constructor
        """
        self.raw = seg_strings


    def __str__(self) -> str:
        """ ToString override
        """
        if self._toStr is None:
            self._toStr  = "[{}] -> {}".format(self.index, self.header)
            self._toStr += "\n  - Uri: {}".format(self._page)
            self._toStr += "\n  - Test True: {}".format(self._iTrue)
            self._toStr += "\n  - Test False: {}".format(self._iFalse)
            self._toStr += "\n  - Referer: {}\n".format(self._referer)
            self._toStr += "\n  - Is SQL: {}".format(self.isSQL)
            self._toStr += "\n  - Is NoSQL: {}".format(self.isNoSQL)
            self._toStr += "\n  - Is Blind: {}".format(self.isBlind)
            self._toStr += "\n  - Element: {}\n".format(self._element)
            self._toStr += "\n  - Injected: {}".format(self._injected)
            self._toStr += "\n  - HTTP:\n      {}".format("\n      ".join(self._http))

            self._toStr += "\n\n"

        return self._toStr


#--------------------------------------------------------------------------------------------[ File Parsing Methods ]--
def check(filename : str) -> list:
    """ Reads the poutput file, stripping blank lines
    """
    #-- Make sure the file exists and is a file
    if not (os.path.exists(filename) and os.path.isfile(filename)):
        if os.path.isdir(filename):
            help("File expected but '{}' is a directory.".format(filename))
        else:
            help("File not found ({})".format(filename))
    
    #-- Read the file contents
    f = open(filename, "r")
    result = []
    for x in f:
        t = x.rstrip()
        if len(t):
            result.append(t)
    f.close()
    
    return result


def chomp(fl : list, i : int) -> tuple:
    """ Returns parsed segments
    """
    result = []
    while (i < len(fl)) and not fl[i].startswith("-----"):
        result.append(fl[i])
        i += 1

    if fl[i].startswith("-----"):
        i += 1

    return (ArachniSegment(result), i)


#--------------------------------------------------------------------------------------------[ Command-line Methods ]--
def splash():
    """ Shows a splash screen
    """
    global SPLASH
    print(SPLASH)


def help(err : str = None) -> None:
    """ Prints the help page
    """
    global FLAGS

    #-- Print error (if any)
    if not err is None:
        print("\n    ERROR:\n        {}".format(err))

    #-- Build Flags help
    xMax = 0
    xKey = []
    for x in FLAGS.keys():
        temp = x

        if not FLAGS[x][1] is None:
            for t in FLAGS[x][1:]:
                temp = "{0} <{1}>".format(temp, t.__name__)

        if xMax < len(temp):
            xMax = len(temp)

        xKey += [(temp, FLAGS[x][0])]

    xFlags = ""
    for x in xKey:
        xFlags += "\n        {}  : {}".format(x[0].ljust(xMax), x[1])

    #-- Print help
    print("""
    USAGE:
        ./{0} [flags] inputfile

    FLAGS:{1}
        
    EXAMPLE:
        ./{0} --print-only "/tmp/arachni.txt"
        ./{0} "/tmp/arachni.txt"
    \n\n""".format(os.path.basename(sys.argv[0]), xFlags))
    exit(0)


def get_flags() -> dict:
    """ Gets the flags passed from the command line.
        Tuple format: ([user defined], <type>[value])
    """
    global FLAGS, FLAG_DEFAULTS

    #-- Check for malformed flags
    for key in sys.argv[1:-1]:
        if not key in FLAGS.keys():
            help("Unknown flag '{}'".format(key))

    #-- Check for help flag (and terminate early)
    if "--help" in sys.argv[1:-1]:
        help()

    #-- Parse args
    result = dict(FLAG_DEFAULTS)
    for key in FLAGS.keys():
        if key in sys.argv[1:]:
            if FLAGS[key][1] is None:
                #-- Set On/Off flag to On (True)
                result[key] = (True, True)
            else:
                #-- Get the parameters value index
                vpos = sys.argv.index(key, 1) + 1
                if (not FLAGS[key][1] is None) and ((vpos == (len(sys.argv) - 1)) or (sys.argv[vpos] in FLAGS.keys())):
                    #-- No value given for flag that expects one
                    help("Value expected for {} but none given.".format(key))
                else:
                    try:
                        #-- Cast parameter to given type
                        value = FLAGS[key][1](sys.argv[vpos])
                        result[key] = (True, value)
                    except ValueError:
                        help("Expected type for flag '{}' is <{}>. ('{}' given)".format(key, FLAGS[key][1].__name__, sys.argv[vpos]))
        else:
            if key in FLAG_DEFAULTS.keys():
                #-- Set as default value
                result[key] = (False, FLAG_DEFAULTS[key])
            else:
                #-- Set On/Off flag to off (False)
                result[key] = (False, False)

    return result


#============================================================================================================[ MAIN ]==
splash()
if len(sys.argv) == 1:
    #-- no input
    help()
else:
    #-- input
    flags = get_flags()

    #-- Read file contents
    fl = []
    try:
        fl = check(sys.argv[-1])
    finally:
        pass

    #-- Parse and process the file
    if len(fl) > 0:
        i = 0
        while i < len(fl):
            a, i = chomp(fl, i)
            print(a)
